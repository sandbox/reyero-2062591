<?php

/**
 * @file
 * Callbacks for adding, editing, and deleting translations and managing revisions.
 *
 * Also includes validation, submission and other helper functions.
 *
 * @see translation_menu()
 */

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\translation\TranslationSetInterface;

/**
 * Page callback: Displays add translations links for available translations types.
 *
 * Redirects to translation/add/[type] if only one translations type is available.
 *
 * @return array
 *   A render array for a list of the translation types that can be added; however, if
 *   there is only one translation type defined for the site, the function redirects
 *   to the translation add page for that one translation type and does not return at all.
 *
 * @see translation_menu()
 */
function translation_set_add_page() {
  $translations = array();
  // Only use translation types the user has access to.
  foreach (translation_type_get_types() as $type) {
    if (translation_set_access('create', $type->type)) {
      $translations[$type->type] = $type;
    }
  }
  // Bypass the translation/add listing if only one translations type is available.
  if (count($translations) == 1) {
    $type = array_shift($translations);
    return new RedirectResponse(url('translation/add/' . $type->type, array('absolute' => TRUE)));
  }
  return array('#theme' => 'translation_add_list', '#translations' => $translations);
}

/**
 * Returns HTML for a list of available translation types for translation creation.
 *
 * @param $variables
 *   An associative array containing:
 *   - translations: An array of translations types.
 *
 * @see translation_add_page()
 *
 * @ingroup themeable
 */
function theme_translation_add_list($variables) {
  $translations = $variables['translations'];

  if ($translations) {
    $output = '<dl class="translation-type-list">';
    foreach ($translations as $type) {
      $output .= '<dt>' . l($type->name, 'translation/add/' . $type->type) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($type->description) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    $output = '<p>' . t('You have not created any translations types yet. Go to the <a href="@create-translations">translations type creation page</a> to add a new translations type.', array('@create-translations' => url('admin/structure/translation/add'))) . '</p>';
  }
  return $output;
}


/**
 * Page callback: Provides the translation submission form.
 *
 * @param $translation_type
 *   The translation type object for the submitted translation.
 *
 * @return array
 *   A translation submission form.
 *
 * @see translation_menu()
 */
function translation_set_add($translation_type) {
  global $user;
  $type = $translation_type->type;
  $langcode = module_invoke('language', 'get_default_langcode', 'translation_set', $type);
  $translation = entity_create('translation_set', array(
    'type' => $type,
    'source_langcode' => $langcode ? $langcode : language_default()->id,
  )); //->getBCEntity();
  drupal_set_title(t('Create @name', array('@name' => $translation_type->name)), PASS_THROUGH);

  return Drupal::entityManager()->getForm($translation);
}

