<?php

/**
 * @file
 * Contains \Drupal\translation\TranslationTypeFormController.
 */

namespace Drupal\translation;

use Drupal\Core\Entity\EntityFormController;
use Drupal\Core\Entity\EntityControllerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for translation type forms.
 */
class TranslationTypeFormController extends EntityFormController implements EntityControllerInterface {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, array &$form_state) {
    $form = parent::form($form, $form_state);
    $type = $this->entity;
    if ($this->operation == 'add') {
      drupal_set_title(t('Add translation type'));
    }
    elseif ($this->operation == 'edit') {
      drupal_set_title(t('Edit %label translation type', array('%label' => $type->label())), PASS_THROUGH);
    }

    $translation_settings = $type->getModuleSettings('translation');
    // Ensure default settings.
    $translation_settings += array(
      'options' => array('status', 'promote'),
      'preview' => DRUPAL_OPTIONAL,
      'submitted' => TRUE,
    );

    $form['name'] = array(
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#default_value' => $type->name,
      '#description' => t('The human-readable name of this translation type. '),
      '#required' => TRUE,
      '#size' => 30,
    );

    $form['type'] = array(
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#maxlength' => 32,
      '#disabled' => $type->isLocked(),
      '#machine_name' => array(
        'exists' => 'translation_type_load',
        'source' => array('name'),
      ),
      '#description' => t('A unique machine-readable name for this translation type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %translation-add page, in which underscores will be converted into hyphens.', array(
        '%translation-add' => t('Add new translation'),
      )),
    );

    $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $type->description,
      '#description' => t('Describe this translation type. The text will be displayed on the <em>Add new translation</em> page.'),
    );

    $form['has_title'] = array(
      '#title' => t('Enable title field'),
      '#type' => 'checkbox',
      '#default_value' => $type->has_title,
    );
    $form['additional_settings'] = array(
      '#type' => 'vertical_tabs',
      '#attached' => array(
        'library' => array(array('translation', 'drupal.translation_types')),
      ),
    );

    $form['workflow'] = array(
      '#type' => 'details',
      '#title' => t('Publishing options'),
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
    );
    $form['workflow']['options'] = array('#type' => 'checkboxes',
      '#title' => t('Default options'),
      '#parents' => array('settings', 'translation', 'options'),
      '#default_value' => $translation_settings['options'],
      '#options' => array(
        'status' => t('Published'),
      ),
      '#description' => t('Users with the <em>Administer translations</em> permission will be able to override these options.'),
    );
    $form['display'] = array(
      '#type' => 'details',
      '#title' => t('Display settings'),
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
    );
    $form['display']['submitted'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display author and date information.'),
      '#parents' => array('settings', 'translation', 'submitted'),
      '#default_value' => $translation_settings['submitted'],
      '#description' => t('Author username and publish date will be displayed.'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, array &$form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = t('Save translation type');
    $actions['delete']['#value'] = t('Delete translation type');
    $actions['delete']['#access'] = $this->entity->access('delete');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array $form, array &$form_state) {
    parent::validate($form, $form_state);

    $id = trim($form_state['values']['type']);
    // '0' is invalid, since elsewhere we check it using empty().
    if ($id == '0') {
      form_set_error('type', t("Invalid machine-readable name. Enter a name other than %invalid.", array('%invalid' => $id)));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, array &$form_state) {
    $type = $this->entity;
    $type->type = trim($type->id());
    $type->name = trim($type->name);

    $variables = $form_state['values'];

    // Do not save settings from vertical tabs.
    // @todo Fix vertical_tabs.
    unset($variables['additional_settings__active_tab']);

    // @todo Remove the entire following code after converting translation settings of
    //   Comment and Menu module. https://drupal.org/translation/2026165
    // Remove all translation type entity properties.
    foreach (get_class_vars(get_class($type)) as $key => $value) {
      unset($variables[$key]);
    }
    // Save or reset persistent variable values.
    foreach ($variables as $key => $value) {
      $variable_new = $key . '_' . $type->id();
      $variable_old = $key . '_' . $type->getOriginalID();
      if (is_array($value)) {
        $value = array_keys(array_filter($value));
      }
      variable_set($variable_new, $value);
      if ($variable_new != $variable_old) {
        variable_del($variable_old);
      }
    }
    // Saving the translation type after saving the variables allows modules to act
    // on those variables via hook_translation_type_insert().
    $status = $type->save();

    $t_args = array('%name' => $type->label());

    if ($status == SAVED_UPDATED) {
      drupal_set_message(t('The translation type %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      drupal_set_message(t('The translation type %name has been added.', $t_args));
      watchdog('translation', 'Added translation type %name.', $t_args, WATCHDOG_NOTICE, l(t('view'), 'admin/structure/translation'));
    }

    $form_state['redirect'] = 'admin/structure/translation';
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $form, array &$form_state) {
    $form_state['redirect'] = 'admin/structure/translation/manage/' . $this->entity->id() . '/delete';
  }

}
