<?php

/**
 * @file
 * Contains \Drupal\translation\Entity\TranslationSetInterface.
 */

namespace Drupal\translation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a translation entity.
 */
interface TranslationSetInterface extends ContentEntityInterface {

  /**
   * Returns the node type.
   *
   * @return string
   *   The node type.
   */
  public function getType();

  /**
   *
   * Returns the node title.
   *
   * @return string
   *   Title of the node.
   */
  public function getTitle();

  /**
   * Sets the translation title.
   *
   * @param string $title
   *   The translation title.
   *
   * @return \Drupal\translation\TranslationSetInterface
   *   The called translation entity.
   */
  public function setTitle($title);

  /**
   * Returns the translation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the translation.
   */
  public function getCreatedTime();

  /**
   * Sets the translation creation timestamp.
   *
   * @param int $timestamp
   *   The translation creation timestamp.
   *
   * @return \Drupal\translation\TranslationSetInterface
   *   The called translation entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the translation modification timestamp.
   *
   * @return int
   *   Translation creation timestamp.
   */
  public function getChangedTime();

  /**
   * Returns the translation published status indicator.
   *
   * Unpublished translations are only visible to their authors and to administrators.
   *
   * @return bool
   *   TRUE if the translation is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a translation..
   *
   * @param bool $published
   *   TRUE to set this translation to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\translation\TranslationSetInterface
   *   The called translation entity.
   */
  public function setPublished($published);

}
