<?php

/**
 * @file
 * Contains \Drupal\translation\TranslationTypeAccessController.
 */

namespace Drupal\translation;

use Drupal\Core\Entity\EntityAccessController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an access controller for the translation type entity.
 *
 * @see \Drupal\translation\Entity\TranslationType.
 */
class TranslationTypeAccessController extends EntityAccessController {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, $langcode, AccountInterface $account) {
    if ($operation == 'delete' && $entity->isLocked()) {
      return FALSE;
    }
    return user_access('administer translation types', $account);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return user_access('administer translation types', $account);
  }

}
