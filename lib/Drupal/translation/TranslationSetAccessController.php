<?php

/**
 * @file
 * Contains \Drupal\translation\TranslationSetAccessController.
 */

namespace Drupal\translation;

use Drupal\Core\Entity\EntityAccessController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an access controller for the translation entity.
 *
 * @see \Drupal\translation\Entity\Translation
 */
class TranslationSetAccessController extends EntityAccessController {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, $langcode, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return user_access('access translations', $account);
        break;

      case 'update':
        return user_access("edit translations", $account) || user_access('administer translations', $account);
        break;

      case 'delete':
        return user_access("delete translations", $account) || user_access('administer translations', $account);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return user_access('create translations', $account);
  }

}
