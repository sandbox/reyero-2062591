<?php

/**
 * @file
 * Definition of Drupal\translation\TranslationSetFormController.
 */

namespace Drupal\translation;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFormControllerNG;
use Drupal\Core\Language\Language;

/**
 * Form controller for the translation edit forms.
 */
class TranslationSetFormController extends EntityFormControllerNG {

  /**
   * Default settings for this content/translation type.
   *
   * @var array
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  protected function prepareEntity() {
    $translation = $this->entity;

    // Set up default values, if required.
    $type = entity_load('translation_type', $translation->bundle());
    $this->settings = $type->getModuleSettings('translation');
    $this->settings += array(
      'options' => array('status'),
      'submitted' => TRUE,
    );

    // If this is a new translation, fill in the default values.
    if ($translation->isNew()) {
      foreach (array('status') as $key) {
        // Multistep translation forms might have filled in something already.
        if (!isset($translation->$key)) {
          $translation->$key = (int) in_array($key, $this->settings['options']);
        }
      }

      $translation->setCreatedTime(REQUEST_TIME);
      //$translation->created = REQUEST_TIME;
    }

    $translation->date = format_date($translation->created, 'custom', 'Y-m-d H:i:s O');

  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   */
  public function form(array $form, array &$form_state) {
    $translation = $this->entity;

    $translation_type = translation_type_load($translation->getType());

    if ($this->operation == 'edit') {
      drupal_set_title(t('<em>Edit @type</em> @title', array('@type' => translation_get_type_label($translation), '@title' => $translation->label())), PASS_THROUGH);
    }

    // Override the default CSS class name, since the user-defined translation type
    // name in 'TYPE-translation-form' potentially clashes with third-party class
    // names.
    $form['#attributes']['class'][0] = drupal_html_class('translation-' . $translation->type . '-form');

    // Basic translation information.
    // These elements are just values so they are not even sent to the client.
    foreach (array('trid', 'created', 'type') as $key) {
      $form[$key] = array(
        '#type' => 'value',
        '#value' => isset($translation->$key) ? $translation->$key : NULL,
      );
    }

    // Changed must be sent to the client, for later overwrite error checking.
    $form['changed'] = array(
      '#type' => 'hidden',
      '#default_value' => isset($translation->changed) ? $translation->changed : NULL,
    );

    if ($translation_type->has_title) {
      $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#required' => TRUE,
        '#default_value' => $translation->title,
        '#maxlength' => 255,
        '#weight' => -5,
      );
    }

    $language_configuration = module_invoke('language', 'get_default_configuration', 'translation', $translation->type);
    $form['langcode'] = array(
      '#title' => t('Language'),
      '#type' => 'language_select',
      '#default_value' => $translation->langcode,
      '#languages' => Language::STATE_ALL,
      '#access' => isset($language_configuration['language_show']) && $language_configuration['language_show'],
    );

    $form['date'] = array(
      '#type' => 'textfield',
      '#title' => t('Created on'),
      '#maxlength' => 25,
      '#description' => t('Format: %time. The date format is YYYY-MM-DD and %timezone is the time zone offset from UTC. Leave blank to use the time of form submission.', array('%time' => !empty($translation->date) ? date_format(date_create($translation->date), 'Y-m-d H:i:s O') : format_date($translation->created, 'custom', 'Y-m-d H:i:s O'), '%timezone' => !empty($translation->date) ? date_format(date_create($translation->date), 'O') : format_date($translation->created, 'custom', 'O'))),
      '#default_value' => !empty($translation->date) ? $translation->date : '',
    );

    return parent::form($form, $form_state, $translation);
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::validate().
   */
  public function validate(array $form, array &$form_state) {

    parent::validate($form, $form_state);
  }


  /**
   * Form submission handler for the 'publish' action.
   *
   * @param $form
   *   An associative array containing the structure of the form.
   * @param $form_state
   *   A reference to a keyed array containing the current state of the form.
   */
  public function publish(array $form, array &$form_state) {
    $translation = $this->entity;
    $translation->status = 1;
    return $translation;
  }

  /**
   * Form submission handler for the 'unpublish' action.
   *
   * @param $form
   *   An associative array containing the structure of the form.
   * @param $form_state
   *   A reference to a keyed array containing the current state of the form.
   */
  public function unpublish(array $form, array &$form_state) {
    $translation = $this->entity;
    $translation->status = 0;
    return $translation;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, array &$form_state) {
    $translation = $this->entity;
    $insert = $translation->isNew();
    $translation->save();

    $translation_link = l(t('view'), 'translation/' . $translation->id());
    $watchdog_args = array('@type' => $translation->type, '%title' => $translation->label());
    $t_args = array('@type' => translation_get_type_label($translation), '%title' => $translation->label());

    if ($insert) {
      watchdog('content', '@type: added %title.', $watchdog_args, WATCHDOG_NOTICE, $translation_link);
      drupal_set_message(t('@type %title has been created.', $t_args));
    }
    else {
      watchdog('content', '@type: updated %title.', $watchdog_args, WATCHDOG_NOTICE, $translation_link);
      drupal_set_message(t('@type %title has been updated.', $t_args));
    }

    if ($translation->id()) {
      $form_state['values']['trid'] = $translation->id();
      $form_state['trid'] = $translation->id();
      $form_state['redirect'] = translation_set_access('view', $translation) ? 'translation/' . $translation->id() : '<front>';
    }
    else {
      // In the unlikely case something went wrong on save, the translation will be
      // rebuilt and translation form redisplayed the same way as in preview.
      drupal_set_message(t('The translation set could not be saved.'), 'error');
      $form_state['rebuild'] = TRUE;
    }

    // Clear the page and block caches.
    //cache_invalidate_tags(array('content' => TRUE));
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::delete().
   */
  public function delete(array $form, array &$form_state) {
    $destination = array();
    $query = \Drupal::request()->query;
    if ($query->has('destination')) {
      $destination = drupal_get_destination();
      $query->remove('destination');
    }
    $translation = $this->entity;
    $form_state['redirect'] = array('translation/' . $translation->id() . '/delete', array('query' => $destination));
  }

}
