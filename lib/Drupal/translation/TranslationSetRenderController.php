<?php

/**
 * @file
 * Definition of Drupal\translation\TranslationSetRenderController.
 */

namespace Drupal\translation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRenderController;
use Drupal\entity\Plugin\Core\Entity\EntityDisplay;

/**
 * Render controller for translation sets.
 */
class TranslationSetRenderController extends EntityRenderController {

  /**
   * Overrides Drupal\Core\Entity\EntityRenderController::buildContent().
   */
  public function buildContent(array $entities, array $displays, $view_mode, $langcode = NULL) {
    parent::buildContent($entities, $displays, $view_mode, $langcode);

    foreach ($entities as $entity) {
      // Add the description if enabled.
      $display = $displays[$entity->bundle()];
      if (!empty($entity->description->value) && $display->getComponent('description')) {
        $entity->content['description'] = array(
          '#markup' => check_markup($entity->description->value, $entity->format->value, '', TRUE),
          '#prefix' => '<div class="translation-term-description">',
          '#suffix' => '</div>',
        );
      }
    }
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityRenderController::getBuildDefaults().
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode, $langcode) {
    $return = parent::getBuildDefaults($entity, $view_mode, $langcode);

    // TODO: rename "term" to "translation_term" in theme_translation_term().
    $return['#translation_set'] = $return["#{$this->entityType}"];
    unset($return["#{$this->entityType}"]);

    return $return;
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityRenderController::alterBuild().
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityDisplay $display, $view_mode, $langcode = NULL) {
    parent::alterBuild($build, $entity, $display, $view_mode, $langcode);
    $build['#attached']['css'][] = drupal_get_path('module', 'translation') . '/css/translation.module.css';
    $build['#contextual_links']['translation'] = array('translation', array($entity->id()));
  }

}
