<?php

/**
 * @file
 * Contains \Drupal\translation\Form\DeleteMultiple.
 */

namespace Drupal\translation\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Controller\ControllerInterface;
use Drupal\Core\Entity\EntityManager;
use Drupal\Component\Utility\String;
use Drupal\user\TempStoreFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a translation deletion confirmation form.
 */
class DeleteMultiple extends ConfirmFormBase implements ControllerInterface {

  /**
   * The array of translations to delete.
   *
   * @var array
   */
  protected $translations = array();

  /**
   * The tempstore factory.
   *
   * @var \Drupal\user\TempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The translation storage controller.
   *
   * @var \Drupal\Core\Entity\EntityStorageControllerInterface
   */
  protected $manager;

  /**
   * Constructs a DeleteMultiple form object.
   *
   * @param \Drupal\user\TempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityManager $manager
   *   The entity manager.
   */
  public function __construct(TempStoreFactory $temp_store_factory, EntityManager $manager) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->storageController = $manager->getStorageController('translation');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.tempstore'),
      $container->get('plugin.manager.entity')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'translation_multiple_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return format_plural(count($this->translations), 'Are you sure you want to delete this translation?', 'Are you sure you want to delete these translations?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelPath() {
    return 'admin/content';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state, Request $request = NULL) {
    $this->translations = $this->tempStoreFactory->get('translation_multiple_delete_confirm')->get($GLOBALS['user']->id());
    if (empty($this->translations)) {
      return new RedirectResponse(url($this->getCancelPath(), array('absolute' => TRUE)));
    }

    $form['translations'] = array(
      '#theme' => 'item_list',
      '#items' => array_map(function ($translation) {
        return String::checkPlain($translation->label());
      }, $this->translations),
    );
    return parent::buildForm($form, $form_state, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    if ($form_state['values']['confirm'] && !empty($this->translations)) {
      $this->storageController->delete($this->translations);
      $this->tempStoreFactory->get('translation_multiple_delete_confirm')->delete($GLOBALS['user']->id());
      $count = count($this->translations);
      watchdog('content', 'Deleted @count posts.', array('@count' => $count));
      drupal_set_message(format_plural($count, 'Deleted 1 post.', 'Deleted @count posts.'));
    }
    $form_state['redirect'] = 'admin/content';
  }

}
