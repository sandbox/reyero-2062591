<?php

/**
 * @file
 * Contains \Drupal\translation\Entity\TranslationType.
 */

namespace Drupal\translation\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\translation\TranslationTypeInterface;
use Drupal\Core\Entity\Annotation\EntityType;
use Drupal\Core\Annotation\Translation;

/**
 * Defines the Translation type configuration entity.
 *
 * @EntityType(
 *   id = "translation_type",
 *   label = @Translation("Translation type"),
 *   module = "translation",
 *   controllers = {
 *     "storage" = "Drupal\Core\Config\Entity\ConfigStorageController",
 *     "access" = "Drupal\translation\TranslationTypeAccessController",
 *     "form" = {
 *       "add" = "Drupal\translation\TranslationTypeFormController",
 *       "edit" = "Drupal\translation\TranslationTypeFormController",
 *       "delete" = "Drupal\translation\Form\TranslationTypeDeleteConfirm"
 *     },
 *     "list" = "Drupal\translation\TranslationTypeListController",
 *   },
 *   config_prefix = "translation.type",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class TranslationType extends ConfigEntityBase implements TranslationTypeInterface {

  /**
   * The machine name of this translation type.
   *
   * @var string
   *
   * @todo Rename to $id.
   */
  public $type;

  /**
   * The UUID of the translation type.
   *
   * @var string
   */
  public $uuid;

  /**
   * The human-readable name of the translation type.
   *
   * @var string
   *
   * @todo Rename to $label.
   */
  public $name;

  /**
   * A brief description of this translation type.
   *
   * @var string
   */
  public $description;

  /**
   * Help information shown to the user when creating a Translation of this type.
   *
   * @var string
   */
  public $help;

  /**
   * Indicates whether the Translation entity of this type has a title.
   *
   * @var bool
   *
   * @todo Rename to $translation_has_title.
   */
  public $has_title = TRUE;

  /**
   * Module-specific settings for this translation type, keyed by module name.
   *
   * @var array
   *
   * @todo Pluginify.
   */
  public $settings = array();

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function uri() {
    return array(
      'path' => 'admin/structure/translation/manage/' . $this->id(),
      'options' => array(
        'entity_type' => $this->entityType,
        'entity' => $this,
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleSettings($module) {
    if (isset($this->settings[$module]) && is_array($this->settings[$module])) {
      return $this->settings[$module];
    }
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    $locked = \Drupal::state()->get('translation.type.locked');
    return isset($locked[$this->id()]) ? $locked[$this->id()] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageControllerInterface $storage_controller, $update = TRUE) {
    if (!$update) {
      // Clear the translation type cache, so the new type appears.
      \Drupal::cache()->deleteTags(array('translation_types' => TRUE));

      entity_invoke_bundle_hook('create', 'translation', $this->id());

      // Unless disabled, automatically create a Body field for new translation types.
      if ($this->get('create_body')) {
        $label = $this->get('create_body_label');
        translation_add_body_field($this, $label);
      }
    }
    elseif ($this->getOriginalID() != $this->id()) {
      // Clear the translation type cache to reflect the rename.
      \Drupal::cache()->deleteTags(array('translation_types' => TRUE));

      $update_count = translation_type_update_translations($this->getOriginalID(), $this->id());
      if ($update_count) {
        drupal_set_message(format_plural($update_count,
          'Changed the translation type of 1 post from %old-type to %type.',
          'Changed the translation type of @count posts from %old-type to %type.',
          array(
            '%old-type' => $this->getOriginalID(),
            '%type' => $this->id(),
          )));
      }
      entity_invoke_bundle_hook('rename', 'translation', $this->getOriginalID(), $this->id());
    }
    else {
      // Invalidate the cache tag of the updated translation type only.
      cache()->invalidateTags(array('translation_type' => $this->id()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageControllerInterface $storage_controller, array $entities) {
    // Clear the translation type cache to reflect the removal.
    $storage_controller->resetCache(array_keys($entities));
    foreach ($entities as $entity) {
      entity_invoke_bundle_hook('delete', 'translation', $entity->id());
    }
  }

}
