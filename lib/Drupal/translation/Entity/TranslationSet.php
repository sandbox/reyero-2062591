<?php

/**
 * @file
 * Definition of Drupal\translation\Entity\TranslationSet.
 */

namespace Drupal\translation\Entity;

use Drupal\Core\Entity\EntityNG;
use Drupal\Core\Entity\Annotation\EntityType;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Language\Language;
use Drupal\translation\TranslationSetInterface;


/**
 * Defines the translation set entity.
 *
 * @EntityType(
 *   id = "translation_set",
 *   label = @Translation("Translation"),
 *   bundle_label = @Translation("Type"),
 *   module = "translation",
 *   controllers = {
 *     "storage" = "Drupal\translation\TranslationSetStorageController",
 *     "render" = "Drupal\translation\TranslationSetRenderController",
 *     "access" = "Drupal\translation\TranslationSetAccessController",
 *     "form" = {
 *       "default" = "Drupal\translation\TranslationSetFormController",
 *       "delete" = "Drupal\translation\Form\TranslationSetDeleteForm",
 *       "edit" = "Drupal\translation\TranslationSetFormController"
 *     },
 *   },
 *   base_table = "translation_set",
 *   uri_callback = "translation_set_uri",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "trid",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "label" = "title"
 *   },
 *   bundle_keys = {
 *     "bundle" = "type"
 *   },
 *   links = {
 *     "canonical" = "/translation/{translation_set}",
 *     "edit-form" = "/translation/{translation_set}/edit"
 *   },
 *   menu_base_path = "translation/%translation_set",
 *   route_base_path = "admin/structure/translation/manage/{bundle}",
 *   permission_granularity = "bundle"
 * )
 */
class TranslationSet extends EntityNG implements TranslationSetInterface {

  /**
   * The translation set ID.
   *
   * @var \Drupal\Core\Entity\Field\FieldInterface
   */
  public $trid;

  /**
   * The translation set UUID.
   *
   * @var \Drupal\Core\Entity\Field\FieldInterface
   */
  public $uuid;

  /**
   * Source language code.
   *
   * @var \Drupal\Core\Entity\Field\FieldInterface
   */
  public $source_langcode;

  /**
   * Title of the translation set.
   *
   * @var \Drupal\Core\Entity\Field\FieldInterface
   */
  public $title;

  /**
   * The status of this translation set.
   *
   * @var \Drupal\Core\Entity\Field\FieldInterface
   */
  public $status;

  /**
   * The created timestamp.
   *
   * @var \Drupal\Core\Entity\Field\FieldInterface
   */
  public $created;

  /**
   * The changed timestamp.
   *
   * @var \Drupal\Core\Entity\Field\FieldInterface
   */
  public $changed;

  /**
   * Implements Drupal\Core\Entity\EntityInterface::id().
   */
  public function id() {
    return $this->get('trid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageControllerInterface $storage_controller) {
    // Before saving the translation, set changed and revision times.
    $this->changed->value = REQUEST_TIME;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

}