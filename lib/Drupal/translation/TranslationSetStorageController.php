<?php

/**
 * @file
 * Definition of Drupal\translation\TranslationSetStorageController.
 */

namespace Drupal\translation;

use Drupal\Core\Entity\DatabaseStorageControllerNG;
use Drupal\Core\Entity\EntityInterface;

/**
 * Controller class for translations.
 *
 * This extends the Drupal\Core\Entity\DatabaseStorageController class, adding
 * required special handling for translation entities.
 */
class TranslationSetStorageController extends DatabaseStorageControllerNG {

  /**
   * Overrides Drupal\Core\Entity\DatabaseStorageController::create().
   */
  public function create(array $values) {
    // @todo Handle this through property defaults.
    if (empty($values['created'])) {
      $values['created'] = REQUEST_TIME;
    }
    return parent::create($values);
  }

  /**
   * Overrides \Drupal\Core\Entity\DataBaseStorageControllerNG::basePropertyDefinitions().
   */
  public function baseFieldDefinitions() {
    $properties['trid'] = array(
      'label' => t('Translation ID'),
      'description' => t('The translation set ID.'),
      'type' => 'integer_field',
      'read-only' => TRUE,
    );
    $properties['uuid'] = array(
      'label' => t('UUID'),
      'description' => t('The translation set UUID.'),
      'type' => 'uuid_field',
      'read-only' => TRUE,
    );
    $properties['type'] = array(
      'label' => t('Type'),
      'description' => t('The translation set type.'),
      'type' => 'string_field',
      'read-only' => TRUE,
    );
    $properties['source_langcode'] = array(
      'label' => t('Source language code'),
      'description' => t('The translation set source language code.'),
      'type' => 'language_field',
    );
    $properties['title'] = array(
      'label' => t('Title'),
      'description' => t('The title of this translation, always treated as non-markup plain text.'),
      'type' => 'string_field',
      'required' => TRUE,
      'settings' => array(
        'default_value' => '',
      ),
      'property_constraints' => array(
        'value' => array('Length' => array('max' => 255)),
      ),
    );
    $properties['status'] = array(
      'label' => t('Publishing status'),
      'description' => t('A boolean indicating whether the translation set is published.'),
      'type' => 'boolean_field',
    );
    $properties['created'] = array(
      'label' => t('Created'),
      'description' => t('The time that the translation set was created.'),
      'type' => 'integer_field',
    );
    $properties['changed'] = array(
      'label' => t('Changed'),
      'description' => t('The time that the translation set was last edited.'),
      'type' => 'integer_field',
      'property_constraints' => array(
        'value' => array('TranslationChanged' => array()),
      ),
    );
    return $properties;
  }

}
