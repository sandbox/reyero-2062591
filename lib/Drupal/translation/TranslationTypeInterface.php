<?php

/**
 * @file
 * Contains \Drupal\translation\TranslationTypeInterface.
 */

namespace Drupal\translation;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a translation type entity.
 */
interface TranslationTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the configured translation type settings of a given module, if any.
   *
   * @param string $module
   *   The name of the module whose settings to return.
   *
   * @return array
   *   An associative array containing the module's settings for the translation type.
   *   Note that this can be empty, and default values do not necessarily exist.
   */
  public function getModuleSettings($module);

  /**
   * Returns a locked state of the type.
   *
   * @return string|false
   *   The module name that locks the type or FALSE.
   */
  public function isLocked();

}
