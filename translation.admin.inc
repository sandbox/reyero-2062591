<?php

/**
 * @file
 * Translation administration and module settings user interface.
 */

use Drupal\Core\Language\Language;
use Drupal\translation\TranslationSetInterface;

/**
 * Updates all translations in the passed-in array with the passed-in field values.
 *
 * IMPORTANT NOTE: This function is intended to work when called from a form
 * submission handler. Calling it outside of the form submission process may not
 * work correctly.
 *
 * @param array $translations
 *   Array of translation set trids or translation sets to update.
 * @param array $updates
 *   Array of key/value pairs with translation field names and the value to update that
 *   field to.
 * @param string $langcode
 *   (optional) The language updates should be applied to. If none is specified
 *   all available languages are processed.
 * @param bool $load
 *   (optional) TRUE if $translations contains an array of translation IDs to be loaded, FALSE
 *   if it contains fully loaded translations. Defaults to FALSE.
 */
function translation_mass_update(array $translations, array $updates, $langcode = NULL, $load = FALSE) {
  // We use batch processing to prevent timeout when updating a large number
  // of translations.
  if (count($translations) > 10) {
    $batch = array(
      'operations' => array(
        array('_translation_mass_update_batch_process', array($translations, $updates, $langcode, $load))
      ),
      'finished' => '_translation_mass_update_batch_finished',
      'title' => t('Processing'),
      // We use a single multi-pass operation, so the default
      // 'Remaining x of y operations' message will be confusing here.
      'progress_message' => '',
      'error_message' => t('The update has encountered an error.'),
      // The operations do not live in the .module file, so we need to
      // tell the batch engine which file to load before calling them.
      'file' => drupal_get_path('module', 'translation') . '/translation.admin.inc',
    );
    batch_set($batch);
  }
  else {
    if ($load) {
      $translations = entity_load_multiple('translation', $translations);
    }
    foreach ($translations as $translation) {
      _translation_mass_update_helper($translation, $updates, $langcode);
    }
    drupal_set_message(t('The update has been performed.'));
  }
}

/**
 * Updates individual translations when fewer than 10 are queued.
 *
 * @param \Drupal\translation\TranslationSetInterface $translation
 *   A translation to update.
 * @param array $updates
 *   Associative array of updates.
 * @param string $langcode
 *   (optional) The language updates should be applied to. If none is specified
 *   all available languages are processed.
 *
 * @return \Drupal\translation\TranslationSetInterface
 *   An updated translation object.
 *
 * @see translation_mass_update()
 */
function _translation_mass_update_helper(TranslationSetInterface $translation, array $updates, $langcode = NULL) {
  $langcodes = isset($langcode) ? array($langcode) : array_keys($translation->getTranslationLanguages());
  // For efficiency manually save the original translation before applying any changes.
  $translation->original = clone $translation;
  foreach ($langcodes as $langcode) {
    foreach ($updates as $name => $value) {
      $translation->getTranslation($langcode)->$name = $value;
    }
  }
  $translation->save();
  return $translation;
}

/**
 * Executes a batch operation for translation_mass_update().
 *
 * @param array $translations
 *   An array of translation IDs.
 * @param array $updates
 *   Associative array of updates.
 * @param bool $load
 *   TRUE if $translations contains an array of translation IDs to be loaded, FALSE if it
 *   contains fully loaded translations.
 * @param array $context
 *   An array of contextual key/values.
 */
function _translation_mass_update_batch_process(array $translations, array $updates, $load, array &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($translations);
    $context['sandbox']['translations'] = $translations;
  }

  // Process translations by groups of 5.
  $count = min(5, count($context['sandbox']['translations']));
  for ($i = 1; $i <= $count; $i++) {
    // For each trid, load the translation, reset the values, and save it.
    $translation = array_shift($context['sandbox']['translations']);
    if ($load) {
      $translation = entity_load('translation', $translation);
    }
    $translation = _translation_mass_update_helper($translation, $updates);

    // Store result for post-processing in the finished callback.
    $context['results'][] = l($translation->label(), 'translation/' . $translation->id());

    // Update our progress information.
    $context['sandbox']['progress']++;
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Reports the 'finished' status of batch operation for translation_mass_update().
 *
 * @param bool $success
 *   A boolean indicating whether the batch mass update operation successfully
 *   concluded.
 * @param int $results
 *   The number of translations updated via the batch mode process.
 * @param array $operations
 *   An array of function calls (not used in this function).
 */
function _translation_mass_update_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The update has been performed.'));
  }
  else {
    drupal_set_message(t('An error occurred and processing did not complete.'), 'error');
    $message = format_plural(count($results), '1 item successfully processed:', '@count items successfully processed:');
    $item_list = array(
      '#theme' => 'item_list',
      '#items' => $results,
    );
    $message .= drupal_render($item_list);
    drupal_set_message($message);
  }
}

/**
 * Returns the admin form object to translation_admin_translations().
 *
 * @ingroup forms
 */
function translation_admin_translations() {

  // Build the sortable table header.
  $header = array(
    'title' => array(
      'data' => t('Title'),
      'field' => 't.title',
    ),
    'type' => array(
      'data' => t('Translation type'),
      'field' => 't.type',
      'class' => array(RESPONSIVE_PRIORITY_MEDIUM),
    ),
    'status' => array(
      'data' => t('Status'),
      'field' => 't.status',
    ),
    'changed' => array(
      'data' => t('Updated'),
      'field' => 't.changed',
      'sort' => 'desc',
      'class' => array(RESPONSIVE_PRIORITY_LOW)
    ,)
  );

  $header['operations'] = array('data' => t('Operations'));

  $query = db_select('translation_set', 't')
    ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
    ->extend('Drupal\Core\Database\Query\TableSortExtender');

  if (!user_access('bypass translation access')) {
    // If the user is able to view their own unpublished translations, allow them
    // to see these in addition to published translations. Check that they actually
    // have some unpublished translations to view before adding the condition.
    if (user_access('view own unpublished translations') && $own_unpublished = db_query('SELECT DISTINCT trid FROM {translation_set} WHERE  status = :status', array(':uid' => $GLOBALS['user']->id(), ':status' => 0))->fetchCol()) {
      $query->condition(db_or()
        ->condition('t.status', 1)
        ->condition('t.trid', $own_unpublished, 'IN')
      );
    }
    else {
      // If not, restrict the query to published translations.
      $query->condition('t.status', 1);
    }
  }
  $trids = $query
    ->distinct()
    ->fields('t', array('trid'))
    ->limit(50)
    ->orderByHeader($header)
    ->addTag('translation_access')
    ->execute()
    ->fetchCol();
  $translations = translation_set_load_multiple($trids);

  // Prepare the list of translations.
  $languages = language_list(Language::STATE_ALL);
  $destination = drupal_get_destination();
  $form['translations'] = array(
    '#type' => 'table',
    '#header' => $header,
    '#empty' => t('No translations available.'),
  );
  foreach ($translations as $translation) {
    $l_options = $translation->language()->id != Language::LANGCODE_NOT_SPECIFIED && isset($languages[$translation->language()->id]) ? array('language' => $languages[$translation->language()->id]) : array();
    $form['translations'][$translation->id()]['title'] = array(
      '#type' => 'link',
      '#title' => $translation->label(),
      '#href' => 'translation/' . $translation->id(),
      '#options' => $l_options,
    );
    $form['translations'][$translation->id()]['type'] = array(
      '#markup' => check_plain(translation_get_type_label($translation)),
    );
    $form['translations'][$translation->id()]['status'] = array(
      '#markup' => $translation->isPublished() ? t('published') : t('not published'),
    );
    $form['translations'][$translation->id()]['changed'] = array(
      '#markup' => format_date($translation->getChangedTime(), 'short'),
    );
    if ($multilingual) {
      $form['translations'][$translation->id()]['language_name'] = array(
        '#markup' => $translation->language()->name,
      );
    }

    // Build a list of all the accessible operations for the current translation.
    $operations = array();
    if (translation_set_access('update', $translation)) {
      $operations['edit'] = array(
        'title' => t('Edit'),
        'href' => 'translation/' . $translation->id() . '/edit',
        'query' => $destination,
      );
    }
    if (translation_set_access('delete', $translation)) {
      $operations['delete'] = array(
        'title' => t('Delete'),
        'href' => 'translation/' . $translation->id() . '/delete',
        'query' => $destination,
      );
    }
    if ($translation->isTranslatable()) {
      $operations['translate'] = array(
        'title' => t('Translate'),
        'href' => 'translation/' . $translation->id() . '/translations',
        'query' => $destination,
      );
    }
    $form['translations'][$translation->id()]['operations'] = array();
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $form['translations'][$translation->id()]['operations'] = array(
        '#type' => 'operations',
        '#subtype' => 'translation',
        '#links' => $operations,
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $form['translations'][$translation->id()]['operations'] = array(
        '#type' => 'link',
        '#title' => $link['title'],
        '#href' => $link['href'],
        '#options' => array('query' => $link['query']),
      );
    }
  }

  $form['pager'] = array('#theme' => 'pager');
  return $form;
}
